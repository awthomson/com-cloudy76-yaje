# YAJE - Yet Another JSON Editor

This is a simple, client-side JSON editor/validator.

You can access the live site here: https://yaje.cloudy76.com

Other tools you may find useful:
* SimpleSecret - A secure way to send secrets over the internet https://simplesecret.cloudy76.com
* AWS Policy Generator - A quick way to search across all AWS actions https://aws-policy.cloudy76.com
