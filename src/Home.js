import React from 'react';

import AceEditor from "react-ace";
import "ace-builds/src-noconflict/mode-json";
import "ace-builds/src-noconflict/theme-github";

class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            jsontext: "{}",
			result: "Enter your JSON data in the text field above",
			resultState: "result"
		}
		this.updateJSONText = this.updateJSONText.bind(this);
	}


    updateJSONText(event) {
		console.log(event)
        this.setState({jsontext: event})
    }

	validate = () => {
		var jsontext = this.state.jsontext
		try {
			var obj = JSON.parse(jsontext)
		} catch(err) {
        	this.setState({resultState: "result bad"})
        	this.setState({result: err.message})
			return false
		}
        this.setState({result: "Valid JSON data"})
        this.setState({resultState: "result good"})
		this.setState({jsontext: JSON.stringify(obj, null, 2)})
	}

	render() {
		return (
			<div>
				<div className="head">
					<h1>YAJE - Yet Another JSON Editor</h1>
					This is a light-weight app to quickly edit and lint JSON payloads.<br/>
					Enter the JSON in the editor below.  Press the <b>Validate</b> butten to verify it is valid JSON data.
				</div>
				<AceEditor className="editor"
    				mode="json"
    				theme="github"
					fontSize="14px"
					width="1005"
					value={this.state.jsontext}
					onChange={this.updateJSONText}
				/>
				<div className="buttonbar">
					<button onClick={this.validate}>Validate</button>
				</div>
				<pre className={this.state.resultState} >{this.state.result}</pre>
			</div>
		)
	}
}

export default Home
